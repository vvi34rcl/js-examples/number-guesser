let min = 1;
let max = 10;
let hiddenNum = Math.floor(Math.random() * (max + 1 - min) + min);
let guessesLeft = 3;

const gameBlock = document.getElementById('game');
const minNumEl = document.getElementById('min-num');
const maxNumEl = document.getElementById('max-num');
const guessInp = document.getElementById('guess-inp');
const guessSubmit = document.getElementById('guess-submit');
const message = document.getElementById('message');

minNumEl.textContent = min;
maxNumEl.textContent = max;
guessInp.min = min;
guessInp.max = max;

function setMessage(messageText='', color='') {
    message.style.color = color;
    message.textContent = messageText;
}

function gameOver(messageText, color) {
    guessInp.disabled = true;
    guessInp.style.borderColor = color;
    setMessage(messageText, color);

    guessSubmit.value = 'Play Again';
}

guessSubmit.addEventListener('click', function () {
    if (this.value === 'Play Again') {
        window.location.reload();

        return;
    }

    const guess = parseInt(guessInp.value);

    if (Number.isNaN(guess) || guess < min || guess > max) {
        setMessage(`Please enter a number between ${min} and ${max}`, 'red');

        return;
    }

    if (guess === hiddenNum) {
        gameOver(`${hiddenNum} is correct, YOU WIN!`, 'green');
    } else {
        --guessesLeft;

        if (guessesLeft === 0) {
            gameOver(`Game over, you lost. The correct number was ${hiddenNum}`, 'red');
        } else {
            guessInp.style.borderColor = 'red';
            setMessage(`${guess} is not correct, ${guessesLeft} guesses left`, 'red');
            guessInp.value = '';
            guessInp.focus();
        }
    }
});

guessInp.focus();
